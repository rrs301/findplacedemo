import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import GlobalApi from "../Services/GlobalApi";
import { useEffect, useState } from "react";
import PlaceList from "../components/HomePage/PlaceList";

import CategoryList from "../components/HomePage/CategoryList";
import SearchBar from "../components/HomePage/SearchBar";
import GoogleMapC from "../components/HomePage/GoogleMapC";
import SideNavBar from "../components/HomePage/SideNavBar";
import { BusinessLocationContext } from "../context/BusinessLocationContext";
import { SelectedBusinessContext } from "../context/SelectedBusinessContext";
import BusinessToast from "../components/HomePage/BusinessToast";

export default function Home() {
  const [businessList, setBusinessList] = useState([]);
  const [selectedBusiness, setSelectedBusiness] = useState([]);

  const [userLocation, setUserLocation] = useState();

  useEffect(() => {
    getUserLocation();
  }, []);

  const getUserLocation = () => {
    navigator.geolocation.getCurrentPosition(function (pos) {
      console.log(pos);
      setUserLocation({
        lat: pos.coords.latitude,
        lng: pos.coords.longitude,
      });
      SearchPlace("gas_station");
    });
  };

  const SearchPlace = (value = "gas_station",type='cat') => {
   if(type=='search')
   {
    GlobalApi.SearchPlaceByText(
      value,
      userLocation?.lat,
      userLocation?.lng
    ).then((resp) => {
      console.log(resp.data.candidates)
      setBusinessList(resp?.data?.candidates);
    });
   }
   else{
    GlobalApi.SearchNearByPlaceByText(
      value,
      userLocation?.lat,
      userLocation?.lng
    ).then((resp) => {
      setBusinessList(resp?.data?.results);
    });
  }
  };
  return (
    <div className="flex ">
      <SelectedBusinessContext.Provider
        value={{ selectedBusiness, setSelectedBusiness }}
      >
        <BusinessLocationContext.Provider
          value={{ businessList, setBusinessList }}
        >
          <SideNavBar />
          <div
            className="grid grid-cols-1 md:space-x-9 
   md:grid-cols-2 mt-[50px]  px-6 md:px-10 w-full md:mt-10 "
          >
            {businessList ? (
              <div className="mt-10 md:mt-0">
                <SearchBar setSearchText={(value)=>SearchPlace(value,'search')}/>
                <CategoryList
                  selectedCategory={(value) => SearchPlace(value)}
                />
                <PlaceList businessList={businessList} />
              </div>
            ) : null}
            <div className="order-first md:order-last">
              <GoogleMapC />
             <BusinessToast userLocation={userLocation} />

            </div>
           
          </div>
        
         
        </BusinessLocationContext.Provider>
      </SelectedBusinessContext.Provider>
    </div>
  );
}
