import { ClerkProvider, SignIn, SignedIn, SignedOut, UserButton } from '@clerk/nextjs'
import '../styles/globals.css'

function MyApp({ Component, pageProps }) {
  return (
  <ClerkProvider>
    <SignedIn>
      {/* <UserButton/> */}
    <Component {...pageProps} />
    </SignedIn>
    <SignedOut>
      <div className='backgroundColor:#000 flex justify-center'>
      <SignIn/>
      </div>
     
    </SignedOut>
  </ClerkProvider>
  )
}

export default MyApp
