import { InfoBox, InfoBoxF, MarkerF } from '@react-google-maps/api'
import React, { useContext, useEffect, useState } from 'react'
import { BusinessLocationContext } from '../../context/BusinessLocationContext';
import { SelectedBusinessContext } from '../../context/SelectedBusinessContext';
import { currentUser, useAuth, useUser } from '@clerk/nextjs';

 function Marker({userLocation}) {
    const {businessList,setBusinessList}=useContext(BusinessLocationContext)
    const {selectedBusiness,setSelectedBusiness}=useContext(SelectedBusinessContext)
    console.log(selectedBusiness)
    const [showLabel,setShowLabel]=useState(false)
    const {isLoaded, isSignedIn, user  } = useUser();
    useEffect(()=>{
      console.log(user.imageUrl)
    },[])

    
  return (
    <div>
        {businessList.map((business,index)=>index<5&&(
            <div key={index}>
                <MarkerF
                title="hello"
                position={business.geometry.location}
                onClick={()=>setSelectedBusiness(business)}
                icon={{
                url:'/location-pin.png',
                
                scaledSize: { width: 50, height: 50},
                }}>
          <InfoBox
     
     position={business.geometry.location}
    >
      <div style={{ backgroundColor: 'white', 
      backgroundColor:'#c084fc',
      opacity: 1, padding: 7,color:'white',
    borderRadius:10 }}>
        <div style={{ fontSize: 16, fontColor: `#08233B` }}>
          {business.name}
        </div>
      </div>
    </InfoBox>
        </MarkerF>  
       

            </div>
        ))}
            <MarkerF
                title="hello"
                position={userLocation}
                onClick={()=>setSelectedBusiness(business)}
                iconAnchor={0.5}
                icon={{
                 url:'/user-pin.png',
                 borderRadius: '50%',
                scaledSize: { width: 50, height: 50},
                }}>
                  </MarkerF>
    </div>
  )
}

export default Marker