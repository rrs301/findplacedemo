import React, { useContext, useEffect, useState } from 'react'
import BusinessItem from './BusinessItem'
import { SelectedBusinessContext } from '../../context/SelectedBusinessContext';
import BusinessItemLoader from '../../ShimmerEffect/BusinessItemLoader';

function PlaceList({businessList}) {
    const [count,setCount]=useState(0);
    console.log(businessList)
    const [loader,setLoader]=useState(true)
    const {selectedBusiness,setSelectedBusiness}=useContext(SelectedBusinessContext);
    useEffect(()=>{
      
      setInterval(()=>{
        setLoader(false)
      },2000)
    },[])
    useEffect(()=>{
      setLoader(true)
      setCount(0)
    },[businessList])
  return (
    <div>
        <h2 className='text-[20px] mt-3 font-bold mb-3 flex justify-between
        items-center'>Top NearBy Places
        <span className='flex'>
      {count>0  ?
      
      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" 
        strokeWidth={1.5} stroke="currentColor"   onClick={()=>setCount(count-3)}
        className="w-10 h-10 text-gray-400  hover:text-purple-500
        hover:bg-purple-100 p-2
            cursor-pointer rounded-lg">
  <path strokeLinecap="round"

   strokeLinejoin="round" d="M15.75 19.5L8.25 12l7.5-7.5" />
</svg>:null}
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" 
        viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" 
        className="w-10 h-10 text-gray-400  hover:text-purple-500
        hover:bg-purple-100 p-2
            cursor-pointer rounded-lg"
         onClick={()=>setCount(count+3)}>
  <path strokeLinecap="round" strokeLinejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
</svg>

        </span></h2>
        {businessList.length==0?<h2 className='text-gray-400'>Select Category to Search Places</h2>:null}
        
        {!loader&&businessList.length>0&&businessList.map((item,index)=>index>=count&&index<count+3&&index<5&&(
          <div key={index} onClick={()=>setSelectedBusiness(item)}>
            <BusinessItem business={item} />
          </div>
        ))}
        
        {loader?[1,2,3].map((item,index)=>(
        <BusinessItemLoader key={index} />

        )):null}
    </div>
  )
}

export default PlaceList