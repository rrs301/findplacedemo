import React, { useContext, useEffect, useState } from 'react'
import { SelectedBusinessContext } from '../../context/SelectedBusinessContext'
import Image from 'next/image'
import GlobalApi from '../../Services/GlobalApi';

function BusinessToast({userLocation}) {
    const {selectedBusiness,setSelectedBusiness}=useContext(SelectedBusinessContext)
    const [showToast,setShowToast]=useState(false);
    const [distance,setDistance]=useState();


    useEffect(()=>{
        if(selectedBusiness.name)
        {
            console.log("HERE")
            setShowToast(true);
            calculateDistance(
                selectedBusiness.geometry.location.lat,
                selectedBusiness.geometry.location.lng
                ,userLocation.lat,userLocation.lng)
        }
    
    },[selectedBusiness]);


        const calculateDistance = (lat1, lon1, lat2, lon2) => {
          console.log(lat1, lon1, lat2, lon2)
            const earthRadius = 6371; // in kilometers
        
            const degToRad = (deg) => {
              return deg * (Math.PI / 180);
            };
        
            const dLat = degToRad(lat2 - lat1);
            const dLon = degToRad(lon2 - lon1);
        
            const a =
              Math.sin(dLat / 2) * Math.sin(dLat / 2) +
              Math.cos(degToRad(lat1)) * Math.cos(degToRad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        
            const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        
            const distance = earthRadius * c;
            console.log(distance.toFixed(1))
            setDistance(distance.toFixed(1))
            return distance.toFixed(2); // Return the distance with 2 decimal places
          };
        
          const onDirectionClick=()=>{
            window.open('https://www.google.com/maps/dir/?api=1&origin='+
            userLocation.lat+','+userLocation.lng+'&destination='
            +selectedBusiness.geometry.location.lat
            +','+selectedBusiness.geometry.location.lng+'&travelmode=driving')
          }
  return (
    <div className=' z-20  fixed bottom-5 
     right-5 flex justify-center '>
     {showToast?   <div className='bg-purple-400 flex items-center gap-5 p-5 rounded-2xl'>
            <div>
        <h2 className='text-white font-semibold  text-[20px]' >
            {selectedBusiness.name}</h2>
        <h2 className='text-white'>{distance} Miles Away</h2>
        </div>
        <div className='bg-purple-300 p-5 rounded-xl hover:scale-105
        transition-all cursor-pointer ' onClick={()=>onDirectionClick()}>
        <Image src={'/send.png'}
        alt='send'
        width={20}
        height={20}/>
        </div>
        </div>:null}
    </div>
  )
}

export default BusinessToast