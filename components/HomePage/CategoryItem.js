import Image from 'next/image'
import React from 'react'

function CategoryItem({category}) {
  return (
    <div className='flex flex-col items-center bg-purple-100 p-3
     rounded-2xl hover:scale-105 transition-all cursor-pointer'>
        <Image src={category.icon}
        width={35}
        height={35}
        alt={category.name}
        />
        <h2 className=' text-[12px] text-purple-700'>{category.name}</h2>
    </div>
  )
}

export default CategoryItem