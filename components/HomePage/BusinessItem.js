import Image from "next/image";
import React, { useContext, useEffect, useState } from "react";
import GlobalApi from "../../Services/GlobalApi";
import { SelectedBusinessContext } from "../../context/SelectedBusinessContext";

const PLACEHOLDER =
  "https://media.istockphoto.com/id/1147544807/vector/thumbnail-image-vector-graphic.jpg?s=612x612&w=0&k=20&c=rnCKVbdxqkjlcs3xH87-9gocETqpspHFXu5dIGB4wuM=";
function BusinessItem({ business }) {
  const {selectedBusiness,setSelectedBusiness}=useContext(SelectedBusinessContext)
  const [businessImage,setBusinessImage]=useState([]);
  useEffect(() => {
  //  business? getPlaceImage(business):null;
    getPlaceImage();
  }, []);
  const getPlaceImage = async() => {
   
    const photo_ref=business?.photos[0]?.photo_reference
    // GlobalApi.placePhoto(business?.photos[0]?.photo_reference)
    //  .then(resp=>{
     
        
    //   var binaryData = [];
    //   binaryData.push(resp.data)
    //   console.log(resp.data)
    //   setBusinessImage(URL.createObjectURL(new Blob(binaryData)))
    //   console.log(businessImage)
    // })
    const imageURLQuery = await fetch(GlobalApi.CORS_URL + GlobalApi.BASE_URL
      +"/photo?maxwidth=400&photo_reference="+photo_ref+"&key="+GlobalApi.GOOGLE_API_KEY)
    .then(r => r.blob())
    .catch(console.error);
    const image = URL.createObjectURL(imageURLQuery); //declared earlier
    console.log(image)
    setBusinessImage(image)
      // return '/temp.jpg'
 
    
  };
  return (
    <div className={`flex gap-4 mb-4 p-3  
    border-b-[1px] border-purple-300 items-center hover:bg-purple-50
    hover:scale-105 transition-all cursor-pointer  `} >
    {business?  
    <Image
        src={businessImage?businessImage:'/temp.jpg'}
        width={90}
        height={90}
        alt="BusinessImage"
        blurDataURL="/placegolder.jpg"
        placeholder="blur"
   
        className="rounded-xl object-cover h-[100px] w-[100px]"
      />:null}
      <div>
        <h2 className="text-[20px] font-semibold">{business.name}</h2>
        <h2 className="text-[15px] text-gray-500">{business.formatted_address||business.vicinity}</h2>
        <div className="flex gap-2 items-center">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            fill="currentColor"
            className="w-4 h-4 text-yellow-500"
          >
            <path
              fillRule="evenodd"
              d="M10.788 3.21c.448-1.077 1.976-1.077 2.424 0l2.082 5.007 5.404.433c1.164.093 1.636 1.545.749 2.305l-4.117 3.527 1.257 5.273c.271 1.136-.964 2.033-1.96 1.425L12 18.354 7.373 21.18c-.996.608-2.231-.29-1.96-1.425l1.257-5.273-4.117-3.527c-.887-.76-.415-2.212.749-2.305l5.404-.433 2.082-5.006z"
              clipRule="evenodd"
            />
          </svg>
          <h2 className="text-gray-500">{business.rating}</h2>
        </div>
        
      </div>
    </div>
  );
}

export default BusinessItem;
