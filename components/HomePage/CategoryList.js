import React from 'react'
import CategoryItem from './CategoryItem'

function CategoryList({selectedCategory}) {
    const CategoryListData=[
        {
            id:1,
            name:'Gas Station',
            value:'gas_station',
            icon:'/gas.png'
        },
        {
            id:2,
            name:'Restaurants',
            value:'restaurant',
            icon:'/food.png'
        }
    ]
  return (
    <>
     <h2 className='text-[20px] mt-3 font-bold mb-3
     '>Select Your Fav Category</h2>
   
    <div className='flex gap-6 mb-5'>
       
        {CategoryListData.map((item,index)=>(
            <div key={index} onClick={()=>selectedCategory(item.value)}>
            <CategoryItem category={item} />
            </div>
        ))}
    </div>
    </>
  )
}

export default CategoryList