import { GoogleMap, LoadScript, MarkerF } from '@react-google-maps/api';
import React, { useContext, useEffect, useState } from 'react'

import GlobalApi from '../../Services/GlobalApi';
import Image from 'next/image';
import Marker from './Marker';
import { SelectedBusinessContext } from '../../context/SelectedBusinessContext';
function GoogleMapC() {
  const [userLocation,setUserLocation]=useState();
  const [businessLocation,setBusinessLocation]=useState();
  const {selectedBusiness,setSelectedBusiness}=useContext(SelectedBusinessContext);


  const containerStyle = {
    width: '100%',
    height: '500px',
    borderRadius:20
  };
  
  const containerStyleSmall = {
    width: '100%',
    height: '180px',
    borderRadius:20,
    
  };
  
  const center = {
    lat: -3.745,
    lng: -38.523
  };

  useEffect(()=>{
    getUserLocation();
  },[])

  const getUserLocation=()=>{
  navigator.geolocation.getCurrentPosition(function(pos){
    console.log(pos);
    setUserLocation({
      lat: pos.coords.latitude,
      lng: pos.coords.longitude
    })
  })
  
  }
  return (
    <div className='bg-sky-300 rounded-2xl'>
        <LoadScript
        googleMapsApiKey={GlobalApi.GOOGLE_API_KEY}
      >
        <div className='hidden md:block'>
       {userLocation? <GoogleMap
          mapContainerStyle={containerStyle}
          className="rounded-2xl md:h-screen "
          center={selectedBusiness.length!=0?selectedBusiness?.geometry?.location: userLocation}
          zoom={14}
        >
          <>
          <Marker userLocation={userLocation}  />
          </>
        </GoogleMap>:null}
        </div>
        <div className='block md:hidden'>
       {userLocation? <GoogleMap
          mapContainerStyle={containerStyleSmall}
          className="rounded-2xl md:h-screen "
          center={selectedBusiness!=null?selectedBusiness?.geometry?.location: userLocation}
          zoom={14}
        >
          <>
          <Marker userLocation={userLocation}  />
          </>
        </GoogleMap>:null}
        </div>
        
      </LoadScript>
    </div>
  )
}

export default GoogleMapC