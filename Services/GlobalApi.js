
import axios from "axios";

//https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=35.586048,-80.8484864&radius=1000&types=gas_station&key=AIzaSyAlIDUiTW6M9p6qb7mHsMCvqk0_OMO3MV0

const BASE_URL='https://maps.googleapis.com/maps/api/place'
const CORS_URL="https://cors-anywhere.herokuapp.com/";
const GOOGLE_API_KEY="AIzaSyAlIDUiTW6M9p6qb7mHsMCvqk0_OMO3MV0"
const SearchPlaces=(categoryType,lat,lng)=>axios.get(BASE_URL+"/nearbysearch/json?location="+lat+","+lng+"&radius=1000&types="+categoryType+"&key="+GOOGLE_API_KEY)
const placePhoto=(photo_ref)=>axios.get
(CORS_URL+BASE_URL+"/photo?maxwidth=400&photo_reference="+photo_ref+"&key="+GOOGLE_API_KEY,
{
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Redirects': 'true'
    },
  });
const SearchPlaceByText=(categoryType,lat,lng)=>axios.get(CORS_URL+BASE_URL+"/findplacefromtext/json"+
"?fields=formatted_address,name,rating,opening_hours,geometry,photos"+
"&input="+categoryType+
"&inputtype=textquery"+
"&locationbias=circle:10000@"+lat+","+lng+
"&key="+GOOGLE_API_KEY);
const SearchNearByPlaceByText=(categoryType,lat,lng)=>axios.get(CORS_URL+BASE_URL+"/nearbysearch/json"+
"?fields=formatted_address,name,rating,opening_hours,geometry,photos"+
"&type="+categoryType+
"&location="+lat+","+lng+
"&radius=1000"+
"&key="+GOOGLE_API_KEY);

// const getDistance=(dest,origin)=>axios.get("https://maps.googleapis.com/maps/api/distancematrix/json"+
// "?destinations="+dest+
// "&origins="+origin+
// "&units=imperial"+
// "&key="+GOOGLE_API_KEY)

const getDistance=(request)=>axios.get(CORS_URL+'https://maps.googleapis.com/maps/api/distancematrix/json', request)


// https://maps.googleapis.com/maps/api/place/nearbysearch/json
//   ?keyword=cruise
//   &location=-33.8670522%2C151.1957362
//   &radius=1500
//   &type=restaurant
//   &key=YOUR_API_KEY
export default{
    SearchPlaces,
    placePhoto,
    GOOGLE_API_KEY,
    SearchPlaceByText,
    SearchNearByPlaceByText,
    CORS_URL,
    BASE_URL,
    getDistance
}